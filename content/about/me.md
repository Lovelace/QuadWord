---
title: About Me
---

<!--more-->

I am Sarah (but I like to be called Lovelace), I am 16 years old and I am the
creator and maintainer of QuadWord, here I will write about some things that
might be useful when learning programming and I'll try to explain things the
best I can so you enjoy this website and it is useful for you.

I am awfully sorry for my bad English, I am working hard in improving it and
this website is a perfect opportunity to do so. If you see I commit any errors
at any post or comment, call me out, I'll fix it and learn from it, thank you so
much.
