---
title: Welcome to QuadWord
date: 2022-01-13T22:11:50Z
lastmod: 2022-01-13
author: Lovelace
# avatar: /img/author.jpg
# authorlink: https://author.site
cover: /img/welcome.png
# images:
#   - /img/cover.jpg
categories:
  - Information
tags:
  - Information
# nolastmod: true
draft: false
---

A little summary about what QuadWord is and what I have planned to write about.

## Welcome to QuadWord

Hello everyone, I am Lovelace and welcome to my blog, QuadWord!

### What is QuadWord?

QuadWord is a blog whose main theme is programming. I am a lot into low-level
programming (and a lot of other sections of programming people usually call
"hard"), the main goal of this blog is to demistify those topics, so entries
about C, Assembly, operating systems, compilers and things like that shouldn't
be weird at all. If you would like to learn about those subjects, I'd recommend
you to stay tuned in QuadWord!

### Future Guides

I think the main content of this blog are going to be guides, as I really want
to teach people how to code. The following are a list of ideas I have to start
writing in QuadWord:

- Multithreading from scratch in C.
- Create a C compiler in C.
- Regex from scratch.
- x86 Assembly from scratch.
- Data Structures and Algorithms in C.
- C from scratch.
- Linux from scratch.

And a lot of other things I can think of later - yeah, a lot of those things are
from scratch.

### Why QuadWord?

If there are plenty of resources out there... why bothering about creating a new
website? Well, mainly because I want motivate people to learn how things work
under the hood, to know why and how programs operate, sometimes those little
details can play a huge impact in an application - specially in its performace.
The day you discover pointers, you feel like you just discovered a superpower.

Also, because I would like to guide people who are just starting to learn
programming (even if some of the topics I will write about are not completely
for beginners). I have a huge complex, and it's this: I started coding when I
was 8, a cousin of mine showed an HTML video to me, and since then I haven't
stopped coding. The problem is, I was too young, I didn't know what I was doing,
I was just typing along with a video, I feel I wasted a lot of time learning
things that are pretty useless - or I don't like anymore - like JavaScript,
GameMaker Language, HTML, CSS and languages like that, I would have loved to
start learning Perl, C, Assembly, languages like that.

**Note**: I think everyone's first programming language MUST be assembly, as it
will teach you about computers, as no other language does or - probably - never
will. It is not that hard, it is not as hard as it looks, trust me.
